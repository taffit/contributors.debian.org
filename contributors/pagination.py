from rest_framework.pagination import LimitOffsetPagination

class LimitOffsetPaginationWithUnlimited(LimitOffsetPagination):
    def get_limit(self, request):
        if self.limit_query_param:
            limit = request.query_params.get(self.limit_query_param)
            if limit is not None and limit in ("0", "no", "none"):
                return None
        return super(LimitOffsetPaginationWithUnlimited, self).get_limit(request)
