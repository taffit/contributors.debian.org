from django.contrib import admin
import contributors.models as cmodels

class UserAdmin(admin.ModelAdmin):
    pass
admin.site.register(cmodels.User, UserAdmin)

class IdentifierAdmin(admin.ModelAdmin):
    list_display = ('name', 'type')
    #search_fields = ("person__cn", "person__sn", "person__email", "person__uid")
    pass
admin.site.register(cmodels.Identifier, IdentifierAdmin)

class SourceAdmin(admin.ModelAdmin):
    pass
admin.site.register(cmodels.Source, SourceAdmin)

class UserSourceSettingsAdmin(admin.ModelAdmin):
    pass
admin.site.register(cmodels.UserSourceSettings, UserSourceSettingsAdmin)

class ContributionTypeAdmin(admin.ModelAdmin):
    pass
admin.site.register(cmodels.ContributionType, ContributionTypeAdmin)

class ContributionAdmin(admin.ModelAdmin):
    pass
admin.site.register(cmodels.Contribution, ContributionAdmin)
