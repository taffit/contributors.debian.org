# from django.utils.translation import ugettext as _
from django.conf import settings
from django.utils.timezone import now
from django import http
from django.shortcuts import redirect
from django.views.generic import TemplateView, View
from dc.mixins import VisitorMixin
import contributors.models as cmodels
from contributors.serializers import ContributorSerializer, IdentifierSerializer
from .permissions import IsDD
from rest_framework import viewsets
import requests
import datetime
import os
import json

from django_filters.rest_framework import DjangoFilterBackend


class Contributors(VisitorMixin, TemplateView):
    template_name = "contributors/contributors.html"

    def get_context_data(self, **kw):
        ctx = super(Contributors, self).get_context_data(**kw)

        current_year = now().year
        year = self.kwargs.get("year", current_year)
        year = int(year)

        ctx["people"] = cmodels.AggregatedPerson.objects.filter(until__year=year).order_by("user__full_name").select_related("user")
        ctx["teams"] = cmodels.AggregatedSource.objects.filter(until__year=year).order_by("source__name").select_related("source")

        ctx["year"] = year
        ctx["next_year"] = year + 1 if year < current_year else None
        ctx["prev_year"] = year - 1

        return ctx


class ContributorsFlat(VisitorMixin, TemplateView):
    template_name = "contributors/contributors_flat.html"

    def get_context_data(self, **kw):
        ctx = super(ContributorsFlat, self).get_context_data(**kw)
        # Query all people
        ctx["people"] = cmodels.AggregatedPerson.objects.order_by("user__full_name").select_related("user")
        return ctx


class ContributorViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Export per-person aggregated contribution information
    """
    permission_classes = (IsDD,)
    queryset = cmodels.AggregatedPerson.objects.filter(user__hidden=False).order_by("user__full_name").select_related("user")
    serializer_class = ContributorSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('user__email', 'user__full_name', 'begin', 'until')


class IdentifierViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Export non-hidden identifier information
    """
    permission_classes = (IsDD,)
    queryset = cmodels.Identifier.objects.filter(hidden=False, user__isnull=False, user__hidden=False).order_by("name", "type").select_related("user")
    serializer_class = IdentifierSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('type', 'name', 'user__email', 'user__full_name')


class SourcesFlat(VisitorMixin, TemplateView):
    template_name = "contributors/sources_flat.html"

    def get_context_data(self, **kw):
        ctx = super(SourcesFlat, self).get_context_data(**kw)
        # Query all people
        ctx["teams"] = cmodels.AggregatedSource.objects.order_by("source__name").select_related("source")
        return ctx


class ContributorsNew(VisitorMixin, TemplateView):
    """
    Show contributors, newest first
    """
    template_name = "contributors/contributors_new.html"

    def get_context_data(self, **kw):
        res = super(ContributorsNew, self).get_context_data(**kw)
        res["days"] = int(self.request.GET.get("days", 60))
        res["len"] = int(self.request.GET.get("len", 15))
        cutoff = now() - datetime.timedelta(days=res["days"])
        res["people"] = cmodels.AggregatedPerson.objects\
                               .filter(begin__gte=cutoff)\
                               .filter(user__email__endswith="@users.alioth.debian.org")\
                               .order_by("-begin")\
                               .select_related("user")

        res["people"] = [p for p in res["people"] if (p.until - p.begin).days >= res["len"]]
        return res


class ContributorsMIA(VisitorMixin, TemplateView):
    """
    Show Debian Developers who stopped contributing.
    """
    template_name = "contributors/contributors_mia.html"

    def get_context_data(self, **kw):
        res = super(ContributorsMIA, self).get_context_data(**kw)

        # TODO: cache this, using a new "nm" app to talk to nm.debian.org
        api_key = getattr(settings, "NM_API_KEY", None)
        if api_key is not None:
            # See https://wiki.debian.org/ServicesSSL#python-requests
            bundle = '/etc/ssl/ca-debian/ca-certificates.crt'
            if os.path.exists(bundle):
                r = requests.get("https://nm.debian.org/api/status?status=dd_u,dd_nu", headers={"Api-Key": api_key}, verify=bundle)
            else:
                r = requests.get("https://nm.debian.org/api/status?status=dd_u,dd_nu", headers={"Api-Key": api_key})
            people = frozenset(r.json()["people"].keys())
        else:
            people = None

        res["days"] = int(self.request.GET.get("days", 365 * 5))
        cutoff = now() - datetime.timedelta(days=res["days"])
        res["people"] = cmodels.AggregatedPerson.objects\
                               .filter(until__lte=cutoff)\
                               .filter(user__email__endswith="@debian.org")\
                               .order_by("-until")\
                               .select_related("user")

        if people is not None:
            res["people"] = [p for p in res["people"] if p.user.email in people]

        return res


class MIAQuery(VisitorMixin, TemplateView):
    require_visitor = "dd"
    template_name = "contributors/mia_query.html"

    def load_objects(self):
        super(MIAQuery, self).load_objects()
        self.query = self.request.GET.get("q")
        if self.query:
            self.ids = list(
                    cmodels.Identifier.objects.filter(name__icontains=self.query, hidden=False)
                                              .select_related("user")
                                              .select_related("user__aggregated_person")
                                              .order_by("name")[:101]
            )
        else:
            self.ids = None

    def get(self, request, *args, **kw):
        if self.ids and len(self.ids) == 1 and self.ids[0].user:
            user = self.ids[0].user
            if request.GET.get("to") == "nm" and user.is_dd:
                return redirect(user.get_nm_url())
            else:
                return redirect(user.get_absolute_url())
        return super(MIAQuery, self).get(request, *args, **kw)

    def get_context_data(self, **kw):
        ctx = super(MIAQuery, self).get_context_data(**kw)
        ctx["q"] = self.query or ""
        if self.query:
            if len(self.ids) > 100:
                ctx["truncated"] = True
            elif not self.ids:
                ctx["empty"] = True
            ctx["ids"] = self.ids
        return ctx


class SiteStatus(VisitorMixin, TemplateView):
    template_name = "contributors/site_status.html"

    def get_context_data(self, **kw):
        ctx = super(SiteStatus, self).get_context_data(**kw)

        ctx["sources"] = cmodels.Source.objects.all().order_by("name")

        from django.db.models import Count
        ctx["stats"] = {
            "unassociated": list(cmodels.Identifier.objects.filter(user=None).values("type").annotate(count=Count("type")).order_by("type")),
            "associated": list(cmodels.Identifier.objects.filter(user__isnull=False).values("type").annotate(count=Count("type")).order_by("type")),
        }

        return ctx


class Claim(VisitorMixin, TemplateView):
    template_name = "contributors/claim.html"
    require_visitor = "dd"

    def do_association(self, request, person, type, ident):
        """
        Perform the association of type:ident to person. Returns a message
        about what happened.
        """
        try:
            person = cmodels.User.objects.get(email=person)
        except cmodels.User.DoesNotExist:
            return {
                "msgclass": "warning",
                type: "User {} does not exist".format(person),
            }

        if type == "fpr":
            ident = ident.replace(" ", "")

        try:
            ident = cmodels.Identifier.objects.get(type=type, name=ident)
        except cmodels.Identifier.DoesNotExist:
            return {
                "msgclass": "warning",
                type: "Identifier {}:{} does not exist".format(type, ident),
            }

        if ident.user is None:
            ident.user = person
            ident.save()
            log_entry = "Associated by {} using the web interface".format(request.user.email)
            person.add_log(log_entry)
            ident.add_log(log_entry)
            return {
                "msgclass": "note",
                type: "Identifier {} has been associated with {}".format(
                    ident.name, person.email),
            }
        else:
            return {
                "msgclass": "warning",
                type: "Identifier {} is already associated with {}".format(
                    ident.name, ident.user.email),
            }

    def post(self, request):
        type = request.POST["type"]
        msg = self.do_association(request, request.POST["person"], type, request.POST["name"])
        context = self.get_context_data(msg=msg)
        return self.render_to_response(context)


class ClaimIdents(VisitorMixin, View):
    require_visitor = "dd"

    def get(self, request, type):
        term = request.GET.get("q", None)
        limit = int(request.GET.get("limit", 20))
        idents = []
        if term:
            q = cmodels.Identifier.objects.filter(type=type, name__icontains=term, user__isnull=True)
            if limit > 0:
                q = q[:limit]
            for i in q:
                idents.append(i.name)
            idents.sort(key=lambda x: x.lower())
        res = http.HttpResponse(content_type="application/json")
        json.dump(idents, res, indent=2)
        return res


class ClaimPeople(VisitorMixin, View):
    require_visitor = "dd"

    def get(self, request):
        term = request.GET.get("q", None)
        limit = int(request.GET.get("limit", 20))
        users = []
        if term:
            q = cmodels.User.objects.filter(email__icontains=term)
            if limit > 0:
                q = q[:limit]
            for i in q:
                users.append(i.email)
            users.sort(key=lambda x: x.lower())
        res = http.HttpResponse(content_type="application/json")
        json.dump(users, res, indent=2)
        return res
