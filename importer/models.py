from typing import Iterable, Optional, Dict, Any, Tuple, Generator
from django.db import models
from django.utils import timezone
from django import http
from django.shortcuts import get_object_or_404
from django.urls import reverse
from contributors.models import Source
from django.core.files.uploadhandler import InMemoryUploadedFile
from debiancontributors import parser
import datetime
import json
import os
import io
import gzip
import contextlib
import time


# Legacy

class Result(models.Model):
    """
    Track an uploaded submission (legacy, now used read only)
    """
    source = models.ForeignKey(Source, related_name="import_results")
    timestamp = models.DateTimeField(auto_now_add=True, help_text="time when this submission was received")
    details = models.TextField(help_text="JSON encoded import results", blank=True)

    @property
    def details_parsed(self):
        return json.loads(self.details)

    def details_formatted(self):
        info = json.loads(self.details)
        return json.dumps(info, indent=1)


# Current

class Stats:
    __slots__ = ("errors",)

    def __init__(
            self,
            errors: Iterable[str] = (),
            ):
        self.errors = list(errors)

    def to_jsonable(self):
        return {
            "errors": self.errors,
        }

    def formatted(self):
        raise NotImplementedError()

    @classmethod
    def create(cls, operation: str) -> "Stats":
        if operation == "receive":
            return ReceiveStats()
        elif operation == "import":
            return ImportStats()
        else:
            raise ValueError("Invalid operation type {}".format(operation))

    @classmethod
    def from_json(cls, operation: str, parsed: Dict[str, Any]) -> "Stats":
        if operation == "receive":
            return ReceiveStats.from_json(parsed)
        elif operation == "import":
            return ImportStats.from_json(parsed)
        else:
            raise ValueError("Invalid operation type {}".format(operation))


class ReceiveStats(Stats):
    __slots__ = ("request", "records_parsed")

    def __init__(
            self,
            request: Optional[Dict[str, Any]] = None,
            records_parsed: int = 0,
            **kw):
        super().__init__(**kw)
        self.request = request if request is not None else {}
        self.records_parsed = records_parsed

    @classmethod
    def from_json(cls, parsed: Dict[str, Any]) -> "ReceiveStats":
        return cls(**parsed)

    def to_jsonable(self):
        res = super().to_jsonable()
        res["request"] = self.request
        res["records_parsed"] = self.records_parsed
        return res

    def formatted(self):
        fd = io.StringIO()
        print("Records parsed:", self.records_parsed, file=fd)
        return fd.getvalue()


class ImportStats(Stats):
    __slots__ = (
            "identifiers_skipped",
            "contributions_processed",
            "contributions_created",
            "contributions_updated",
            "contributions_skipped",
            "records_parsed",
            "records_skipped")

    def __init__(
            self,
            identifiers_skipped: int = 0,
            contributions_processed: int = 0,
            contributions_created: int = 0,
            contributions_updated: int = 0,
            contributions_skipped: int = 0,
            records_parsed: int = 0,
            records_skipped: int = 0,
            **kw):
        super().__init__(**kw)
        self.identifiers_skipped = identifiers_skipped
        self.contributions_processed = contributions_processed
        self.contributions_created = contributions_created
        self.contributions_updated = contributions_updated
        self.contributions_skipped = contributions_skipped
        self.records_parsed = records_parsed
        self.records_skipped = records_skipped

    @classmethod
    def from_json(cls, parsed: Dict[str, Any]) -> "ReceiveStats":
        return cls(**parsed)

    def to_jsonable(self):
        res = super().to_jsonable()
        res["identifiers_skipped"] = self.identifiers_skipped
        res["contributions_processed"] = self.contributions_processed
        res["contributions_created"] = self.contributions_created
        res["contributions_updated"] = self.contributions_updated
        res["contributions_skipped"] = self.contributions_skipped
        res["records_parsed"] = self.records_parsed
        res["records_skipped"] = self.records_skipped
        return res

    def formatted(self):
        fd = io.StringIO()
        print("Identifiers skipped:", self.identifiers_skipped, file=fd)
        print("Contributions processed", self.contributions_processed, file=fd)
        print("Contributions created", self.contributions_created, file=fd)
        print("Contributions updated", self.contributions_updated, file=fd)
        print("Contributions skipped", self.contributions_skipped, file=fd)
        print("Records parsed", self.records_parsed, file=fd)
        print("Records skipped", self.records_skipped, file=fd)
        return fd.getvalue()

#     def test_dump(self, out=sys.stdout):
#         print("Results.source:", self.source.name if self.source else None, file=out)
#         print("Results.code:", self.code, file=out)
#         print("Results.identifiers_skipped:", self.identifiers_skipped, file=out)
#         print("Results.contributions_processed:", self.contributions_processed, file=out)
#         print("Results.contributions_created:", self.contributions_created, file=out)
#         print("Results.contributions_updated:", self.contributions_updated, file=out)
#         print("Results.contributions_skipped:", self.contributions_skipped, file=out)
#         print("Results.records_parsed:", self.records_parsed, file=out)
#         print("Results.records_skipped:", self.records_skipped, file=out)
#         print("Results.elapsed:", self.elapsed, file=out)
#         for e in self.errors:
#             print("Results.errors:", e, file=out)


class SubmissionManager(models.Manager):
    def pending(self):
        return self.filter(completed__isnull=True)


class Submission(models.Model):
    """
    Track an uploaded submission
    """
    source = models.ForeignKey(Source, related_name="submissions")
    method = models.CharField(max_length=32, null=True, blank=True,
                              choices=(
                                  ("replace", "Replace"),
                                  ("extend", "Extend"),
                              ),
                              help_text="requested import method")
    relpath = models.TextField(help_text="Relative pathname of the submitted file", blank=True)
    received = models.DateTimeField(help_text="time when this submission was received")
    completed = models.DateTimeField(
            null=True, blank=True,
            help_text="time when this submission processing was completed")

    objects = SubmissionManager()

    def get_absolute_url(self):
        return reverse("importer_submission", args=[self.pk])

    @contextlib.contextmanager
    def track(self, operation: str):
        log = SubmissionLog(
                submission=self,
                operation=operation,
                since=time.time())
        with log.track() as stats:
            try:
                yield log, stats
            except parser.Fail as e:
                log.result_code = e.code
                errors = getattr(e, "errors", None)
                if errors is not None:
                    stats.errors.extend(errors)
                else:
                    stats.errors.append(e.msg)
            else:
                log.result_code = 200
            finally:
                log.until = time.time()

    @classmethod
    def from_request(
            cls, request: http.HttpRequest, verify_auth_token: bool = True) -> Tuple["Submission", "SubmissionLog"]:
        res = cls()
        res.received = timezone.now().astimezone(timezone.utc)

        source_name = request.POST.get("source", "")
        res.source = get_object_or_404(Source, name=source_name)

        with res.track("receive") as (log, stats):
            # We only support POST
            if request.method != "POST":
                raise parser.Fail(400, "only POST requests are accepted")

            # Log request metadata
            stats.request = {
                "meta": {k: str(v) for k, v in request.META.items()}
            }

            # Get batch import method name
            import_method = parser.get_key_string(request.POST, "method", True)
            if not import_method:
                import_method = "replace"
            if import_method not in ("replace", "extend"):
                raise parser.Fail(400, "import method {} is not one of (replace, extend)".format(import_method))
            res.method = import_method

            # Validate the auth token
            if verify_auth_token:
                auth_token = parser.get_key_string(request.POST, "auth_token")
                if res.source.auth_token != auth_token:
                    raise parser.Fail(403, "Authentication token is not correct")

            # Save submission content
            # Uncompress and json-decode the submission data from a POST request

            # Validate the data
            data = request.FILES.get("data")
            if data is None:
                raise parser.Fail(400, "no file was posted in a 'data' field")

            # Detect compression type guessing it on uploaded file name
            file_ext = os.path.splitext(getattr(data, 'name', ''))[1]
            guess = file_ext.replace(os.path.extsep, '') or True
            compression = parser.get_key_string(request.POST, "data_compression", guess)

            # Decode the data
            if isinstance(data, InMemoryUploadedFile):
                parsed = parser.get_json(io.BytesIO(data.read()), compression)
            else:
                with open(data.temporary_file_path(), "rb") as fd:
                    parsed = parser.get_json(fd, compression)

            # Validate the submission contents
            dcparser = parser.Parser()
            records_parsed = 0
            try:
                for _ in dcparser.parse_submission(parsed):
                    records_parsed += 1
            except parser.Fail as e:
                errors = getattr(e, "errors", None)
                if errors is not None:
                    stats.errors.extend(errors)
                else:
                    stats.errors.append(e.msg)
            stats.records_parsed = records_parsed

            # Save the submission
            res.relpath = "{:%Y%m%d-%H:%M:%S}.json.gz".format(res.received)

            # Write the contributions log
            with gzip.open(res.get_pathname(create=True), "wt") as fd:
                json.dump(parsed, fd)

        return res, log

    def get_pathname(self, create=False) -> str:
        """
        Get the absolute pathname of the submitted file
        """
        return os.path.join(self.source.get_submission_log_dir(create=create), self.relpath)

    def get_contributions(self) -> Generator:
        """
        Parse the submitted contribution file
        """
        dcparser = parser.Parser()
        with gzip.open(self.get_pathname(), "rt") as fd:
            yield from dcparser.parse_submission(json.load(fd))


class SubmissionLog(models.Model):
    """
    Information about one processing step of a submission
    """
    submission = models.ForeignKey(Submission, related_name="log")
    operation = models.CharField(max_length=32, choices=(
                                     ("receive", "Receive"),
                                     ("import", "Import"),
                                 ),
                                 help_text="operation type")
    since = models.FloatField(help_text="unix timestamp when this operation started")
    until = models.FloatField(help_text="unix timestamp when this operation ended")
    result_code = models.IntegerField(help_text="result code for this operation")
    stats = models.TextField(help_text="JSON encoded operation statistics", blank=True)

    class Meta:
        ordering = ["since"]

    @property
    def since_dt(self):
        return datetime.datetime.fromtimestamp(self.since, timezone.utc)

    @property
    def until_dt(self):
        return datetime.datetime.fromtimestamp(self.until, timezone.utc)

    @property
    def elapsed(self):
        return self.until - self.since

    @property
    def stats_parsed(self):
        if not self.stats:
            return Stats.create(self.operation)
        return Stats.from_json(self.operation, json.loads(self.stats))

    @contextlib.contextmanager
    def track(self):
        stats = self.stats_parsed
        try:
            yield stats
        finally:
            self.stats = json.dumps(stats.to_jsonable())

    def to_response(self, request=None):
        response = http.HttpResponse(content_type="application/json", status=self.result_code)
        stats = json.loads(self.stats) if self.stats else {}
        # Skip request details in response
        stats.pop("request", None)
        jsonable = {
            "submission": {
                "source": self.submission.source.name,
                "method": self.submission.method,
                "relpath": self.submission.relpath,
                "received": self.submission.received.strftime("%Y-%m-%d %H:%M:%S"),
                "completed":
                    self.submission.completed.strftime("%Y-%m-%d %H:%M:%S") if self.submission.completed else None,
            },
            "result_code": self.result_code,
            "operation": self.operation,
            "since": self.since,
            "until": self.until,
            "stats": stats,
        }
        if self.submission.pk is not None:
            submission_url = self.submission.get_absolute_url()
            if request:
                submission_url = request.build_absolute_uri(submission_url)
            jsonable["submission"]["url"] = submission_url
        json.dump(jsonable, response, indent=2)
        return response
