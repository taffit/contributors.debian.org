from django.core.management.base import BaseCommand
from contributors import models as cmodels
import sys
import logging
import json

log = logging.getLogger(__name__)


def read_from_fd(fd):
    res = json.load(fd)
    if isinstance(res, dict):
        return [res]
    else:
        return res


class Command(BaseCommand):
    help = 'Import data source information'
    def add_arguments(self, parser):
        parser.add_argument(
            '--quiet',
            action='store_true',
            dest='quiet',
            default=None,
            help='Disable progress reporting',
            )

    def handle(self, *args, **opts):
        FORMAT = "%(asctime)-15s %(levelname)s %(message)s"
        if opts["quiet"]:
            logging.basicConfig(level=logging.WARNING, stream=sys.stderr, format=FORMAT)
        else:
            logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)

        if args:
            data = []
            for fname in args:
                with open(fname, "rt") as fd:
                    data.extend(read_from_fd(fd))
        else:
            data = read_from_fd(sys.stdin)

        cmodels.Source.import_json(data)
