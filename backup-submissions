#!/usr/bin/env python3

import os
import sys
import re
import io
import shutil
import tarfile
import gzip
import argparse
import logging

log = logging.getLogger()

ROOT = "/srv/contributors.debian.org/dc"
SUBMISSIONS = os.path.join(ROOT, "data/submissions/")
SOURCES = os.path.join(ROOT, "data/contrib-backups/")
BACKUP_ROOT = "/srv/contributors.debian.org/backup/"

class Fail(Exception):
    pass

class Archiver:
    # Root directory under which to look for files to archive
    SOURCE = None
    # Directory under BACKUP_ROOT where we write backup files
    BACKUP_PREFIX = None

    def __init__(self, month, dry_run):
        # Destination archive file
        self.dest = os.path.join(BACKUP_ROOT, self.BACKUP_PREFIX, month + ".tar.xz")
        if os.path.exists(self.dest):
            raise Fail("{} already exists".format(self.dest))
        # Source directory
        self.source = self.SOURCE
        # Month file prefix string
        self.month = month
        # If True, simulate only
        self.dry_run = dry_run

    def get_files_to_backup(self):
        """
        Generate the list of files to back up for a given month
        """
        for root, dirs, files in os.walk(self.source):
            for f in files:
                fmonth = self.extract_month(f)
                if not fmonth or fmonth != self.month: continue
                pathname = os.path.join(root, f)
                if not pathname.startswith(self.source):
                    raise Fail("Computed pathname {} does not start with {}".format(pathname, self.source))
                yield pathname[len(self.source):]

    def add_file(self, tarout, relname):
        """
        Add a file to the archive
        """
        pathname = os.path.join(self.source, relname)
        if relname.endswith(".gz"):
            # Add the decompressed version of the file to the archive
            tarinfo = tarout.gettarinfo(pathname, relname[:-3])
            # Read into a BytesIO so we can compute the size to give to tarfile
            buf = io.BytesIO()
            with gzip.open(pathname, "rb") as infd:
                shutil.copyfileobj(infd, buf)
            # Amend the size of the tarinfo, otherwise we get a truncated file
            # in the tar
            tarinfo.size = len(buf.getvalue())
            # Rewind and write out
            buf.seek(0)
            tarout.addfile(tarinfo, buf)
        else:
            tarinfo = tarout.gettarinfo(pathname, relname)
            with io.open(pathname, "rb") as infd:
                tarout.addfile(tarinfo, infd)
        return tarinfo

    def archive(self):
        """
        Perform archival"
        """
        log.info("%s: started", self.dest)
        files = list(self.get_files_to_backup())
        files.sort()
        src_size = 0
        if self.dry_run:
            for relname in files:
                log.debug("%s: adding %s", self.dest, relname)
            dst_size = 0
        else:
            with tarfile.open(self.dest, "w:xz") as tarout:
                for relname in files:
                    log.debug("%s: adding %s", self.dest, relname)
                    ti = self.add_file(tarout, relname)
                    src_size += ti.size
            dst_size = os.path.getsize(self.dest)

        log.info("%s: %d bytes read, %d bytes written.", self.dest, src_size, dst_size)
        for f in files:
            pathname = os.path.join(self.source, f)
            if not self.dry_run: os.unlink(pathname)
            log.debug("%s: deleted", pathname)
        log.info("%s: %d submissions archived", self.dest, len(files))

    @classmethod
    def get_months_to_backup(cls, keep_months=2):
        """
        Return the month file prefixes of months that need backing up
        """
        months = set()
        for root, dirs, files in os.walk(cls.SOURCE):
            for f in files:
                month = cls.extract_month(f)
                if not month: continue
                months.add(month)
        return sorted(months)[:-keep_months]

    @classmethod
    def prepare_environment(cls, dry_run):
        """
        Perform consistency checks on the environment and create output directories
        """
        # Consistency checks, to ensure we are running in the right place
        if not os.path.isdir(cls.SOURCE):
            raise Fail("{} not found on this host".format(cls.SOURCE))

        # Make sure that we have the backup directories
        if not dry_run:
            os.makedirs(os.path.join(BACKUP_ROOT, cls.BACKUP_PREFIX), exist_ok=True)

    @classmethod
    def run(cls, dry_run, keep_months=2):
        """
        Perform all archival operations: decide which months to archive, create
        output directories, find files, archive them, delete them.
        """
        cls.prepare_environment(dry_run)
        for month in cls.get_months_to_backup(keep_months):
            log.debug("%s: %s", cls.SOURCE, month)
            archiver = cls(month, dry_run)
            archiver.archive()


class SubmissionsArchiver(Archiver):
    SOURCE = SUBMISSIONS
    BACKUP_PREFIX = "submissions"

    @classmethod
    def extract_month(cls, fname):
        """
        Extract the month string from a file name. Returns None if the file name
        does not look like a submission.
        """
        mo = re.match(r"(\d{6})\d{2}-\d\d:\d\d:\d\d.json.gz$", fname)
        if not mo: return None
        return mo.group(1)

class SourcesArchiver(Archiver):
    SOURCE = SOURCES
    BACKUP_PREFIX = "sources"

    @classmethod
    def extract_month(cls, fname):
        """
        Extract the month string from a file name. Returns None if the file name
        does not look like a submission.
        """
        mo = re.match(r"(\d{6})\d{2}.json", fname)
        if not mo: return None
        return mo.group(1)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Back up old submissions.')
    parser.add_argument("--quiet", "-q", action="store_true", help="only print warnings and errors")
    parser.add_argument("--verbose", "-v", action="store_true", help="verbose output")
    parser.add_argument("--dry-run", action="store_true", help="do everything except write operations")
    args = parser.parse_args()

    FORMAT = "%(message)s"
    if args.quiet:
        logging.basicConfig(level=logging.WARNING, stream=sys.stderr, format=FORMAT)
    elif args.verbose:
        logging.basicConfig(level=logging.DEBUG, stream=sys.stderr, format=FORMAT)
    else:
        logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)

    try:
        # Run all archivers
        for archiver in (SourcesArchiver, SubmissionsArchiver):
            archiver.run(args.dry_run)

        sys.exit(0)
    except Fail as e:
        log.error("%s", e)
        sys.exit(1)
