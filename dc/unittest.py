import contributors.models as cmodels
from django.test import Client, override_settings
from rest_framework.test import APIClient
from model_mommy import mommy
import contextlib
import datetime
import re
import six


class NamedObjects(dict):
    """
    Container for fixture model objects.
    """
    def __init__(self, model, skip_delete_all=False, **defaults):
        super(NamedObjects, self).__init__()
        self._model = model
        self._defaults = defaults
        self._skip_delete_all = skip_delete_all

    def __getitem__(self, key):
        """
        Dict that only looks things up if they are strings, otherwise just return key.

        This allows to use __getitem__ with already resolved objects, just to have
        functions that can take either objects or their fixture names.
        """
        if not isinstance(key, str):
            return key
        return super(NamedObjects, self).__getitem__(key)

    def __getattr__(self, key):
        """
        Make dict elements also appear as class members
        """
        res = self.get(key, None)
        if res is not None:
            return res
        raise AttributeError("member {} not found".format(key))

    def _update_kwargs_with_defaults(self, _name, kw):
        """
        Update the kw dict with defaults from self._defaults.

        If self._defaults for an argument is a string, then calls .format() on
        it passing _name and self._defaults as format arguments.
        """
        for k, v in self._defaults.items():
            if isinstance(v, six.string_types):
                kw.setdefault(k, v.format(_name=_name, **self._defaults))
            elif hasattr(v, "__call__"):
                kw.setdefault(k, v(_name, **self._defaults))
            else:
                kw.setdefault(k, v)

    def create(self, _name, **kw):
        self._update_kwargs_with_defaults(_name, kw)
        self[_name] = o = mommy.make(self._model, **kw)
        return o

    def refresh(self):
        """
        Reload all the objects from the database.

        This is needed because although Django's TestCase rolls back the
        database after a test, the data stored in memory in the objects stored
        in NamedObjects repositories is not automatically refreshed.
        """
        for o in self.values():
            o.refresh_from_db()

    def delete_all(self):
        """
        Call delete() on all model objects registered in this dict.

        This can be used in methods like tearDownClass to remove objects common
        to all tests.
        """
        if self._skip_delete_all:
            return
        for o in self.values():
            o.delete()


class TestUsers(NamedObjects):
    def __init__(self, **defaults):
        defaults.setdefault("email", "{_name}@example.org")
        defaults.setdefault("full_name", "{_name}")
        super(TestUsers, self).__init__(cmodels.User, **defaults)

    def create_user(self, _name, **kw):
        self._update_kwargs_with_defaults(_name, kw)
        self[_name] = o = self._model.objects.create_user(**kw)
        return o

    def create_superuser(self, _name, **kw):
        self._update_kwargs_with_defaults(_name, kw)
        self[_name] = o = self._model.objects.create_superuser(**kw)
        return o


class TestMeta(type):
    def __new__(cls, name, bases, attrs):
        res = super(TestMeta, cls).__new__(cls, name, bases, attrs)
        if hasattr(res, "__add_extra_tests__"):
            res.__add_extra_tests__()
        return res


@six.add_metaclass(TestMeta)
class TestBase(object):
    @classmethod
    def _add_method(cls, meth, *args, **kw):
        """
        Add a test method, made of the given method called with the given args
        and kwargs.

        The method name and args are used to built the test method name, the
        kwargs are not: make sure you use the args to make the test case
        unique, and the kwargs for things you do not want to appear in the test
        name, like the expected test results for those args.
        """
        name = re.sub(r"[^0-9A-Za-z_]", "_", "{}_{}".format(meth.__name__.lstrip("_"), "_".join(str(x) for x in args)))
        setattr(cls, name, lambda self: meth(self, *args, **kw))

    @classmethod
    def setUpClass(cls):
        super(TestBase, cls).setUpClass()
        cls._object_repos = []

    @classmethod
    def tearDownClass(cls):
        super(TestBase, cls).tearDownClass()
        for r in cls._object_repos[::-1]:
            r.delete_all()

    def setUp(self):
        super(TestBase, self).setUp()
        for r in self._object_repos:
            r.refresh()

    @classmethod
    def add_named_objects(cls, **kw):
        for name, repo in kw.items():
            cls._object_repos.append(repo)
            setattr(cls, name, repo)

    def make_test_client(self, user, **kw):
        """
        Instantiate a test client, logging in the given person.

        If person is None, visit anonymously. If person is None but
        sso_username is not None, authenticate as the given sso_username even
        if a Person record does not exist.
        """
        user = self.users[user]
        if user is not None:
            kw["SSL_CLIENT_S_DN_CN"] = user.email
        client = Client(**kw)
        client.visitor = user
        return client

    def make_test_apiclient(self, user, **kw):
        """
        Instantiate a test client, logging in the given person.

        If person is None, visit anonymously. If person is None but
        sso_username is not None, authenticate as the given sso_username even
        if a Person record does not exist.
        """
        user = self.users[user]
        if user is not None:
            kw["SSL_CLIENT_S_DN_CN"] = user.email
        client = APIClient(**kw)
        client.visitor = user
        return client

    def assertPermissionDenied(self, response):
        if response.status_code == 403:
            pass
        else:
            self.fail("response has status code {} instead of a 403 Forbidden".format(response.status_code))

    def assertRedirectMatches(self, response, target):
        if response.status_code != 302:
            self.fail("response has status code {} instead of a Redirect".format(response.status_code))
        if target and not re.search(target, response["Location"]):
            self.fail("response redirects to {} which does not match {}".format(response["Location"], target))

    def assertFormErrorMatches(self, response, form_name, field_name, regex):
        form = response.context[form_name]
        errors = form.errors
        if not errors:
            self.fail("Form {} has no errors".format(form_name))
        if field_name not in errors:
            self.fail("Form {} has no errors in field {}".format(form_name, field_name))
        match = re.compile(regex)
        for errmsg in errors[field_name]:
            if match.search(errmsg):
                return
        self.fail("{} dit not match any in {}".format(regex, repr(errors)))

    def assertContainsElements(self, response, elements, *names):
        """
        Check that the response contains only the elements in `names` from PageElements `elements`
        """
        want = set(names)
        extras = want - set(elements.keys())
        if extras:
            raise RuntimeError("Wanted elements not found in the list of possible ones: {}".format(", ".join(extras)))
        should_have = []
        should_not_have = []
        content = response.content.decode("utf-8")
        for name, regex in elements.items():
            if name in want:
                if not regex.search(content):
                    should_have.append(name)
            else:
                if regex.search(content):
                    should_not_have.append(name)
        if should_have or should_not_have:
            msg = []
            if should_have:
                msg.append("should have element(s) {}".format(", ".join(should_have)))
            if should_not_have:
                msg.append("should not have element(s) {}".format(", ".join(should_not_have)))
            self.fail("page " + " and ".join(msg))


class BaseFixtureMixin(TestBase):
    @classmethod
    def setUpClass(cls):
        super(BaseFixtureMixin, cls).setUpClass()
        cls.add_named_objects(
            users=TestUsers(),
            sources=NamedObjects(cmodels.Source),
            ctypes=NamedObjects(cmodels.ContributionType),
            idents=NamedObjects(cmodels.Identifier),
            contributions=NamedObjects(cmodels.Contribution),
        )

    @contextlib.contextmanager
    def login(self, user):
        if user is None:
            with override_settings(TEST_USER=None):
                yield
        else:
            with override_settings(TEST_USER=self.users[user].email):
                yield


class UserFixtureMixin(BaseFixtureMixin):
    """
    Pre-create some users
    """
    @classmethod
    def setUpClass(cls):
        super(UserFixtureMixin, cls).setUpClass()
        cls.users.create_superuser("admin", email="admin@debian.org")
        cls.users.create_user("dd", email="dd@debian.org")
        cls.users.create_user("dd1", email="dd1@debian.org")
        cls.users.create_user("alioth", email="alioth-guest@users.alioth.debian.org")
        cls.users.create_user("alioth1", email="alioth1-guest@users.alioth.debian.org")


class SourceFixtureMixin(UserFixtureMixin):
    @classmethod
    def setUpClass(cls):
        super(SourceFixtureMixin, cls).setUpClass()

        cls.sources.create(
                "test", name="test", desc="test source", url="http://www.example.org", auth_token="testsecret")
        cls.ctypes.create(
                "tester", source=cls.sources.test, name="tester", desc="tester_desc", contrib_desc="tester_cdesc")

        dates = {
            "begin": datetime.date(2014, 2, 1),
            "until": datetime.date(2014, 2, 15),
        }
        for u in "admin", "dd", "dd1", "alioth", "alioth1":
            # Build two IDs per user, one matching the user and one
            # NAME@example.org
            user = cls.users[u]

            id1 = "{}_user".format(u)
            cls.idents.create(id1, type="email", name=user.email, user=user)

            id2 = "{}_home".format(u)
            home_email = re.sub(r"(?:-guest)?@.+", "@example.org", user.email)
            cls.idents.create(id2, type="email", name=home_email, user=user)

            # Make a contribution for each identifier
            cls.contributions.create(id1, type=cls.ctypes.tester, identifier=cls.idents[id1], **dates)
            cls.contributions.create(id2, type=cls.ctypes.tester, identifier=cls.idents[id2], **dates)


class PageElements(dict):
    """
    List of all page elements possibly expected in the results of a view.

    dict matching name used to refer to the element with regexp matching the
    element.
    """
    def add_id(self, id):
        self[id] = re.compile(r"""id\s*=\s*["']{}["']""".format(re.escape(id)))

    def add_class(self, cls):
        self[cls] = re.compile(r"""class\s*=\s*["']{}["']""".format(re.escape(cls)))

    def add_href(self, name, url):
        self[name] = re.compile(r"""href\s*=\s*["']{}["']""".format(re.escape(url)))

    def add_string(self, name, term):
        self[name] = re.compile(r"""{}""".format(re.escape(term)))

    def clone(self):
        res = PageElements()
        res.update(self.items())
        return res
