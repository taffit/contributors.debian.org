from django.conf.urls import include, url
from django.views.generic import TemplateView
from contributors import views as cviews
import sources.urls as sources_urls
from rest_framework import routers

from django.contrib import admin
admin.autodiscover()

router = routers.DefaultRouter()
router.register(r'contributors', cviews.ContributorViewSet, "contributors")
router.register(r'identifiers', cviews.IdentifierViewSet, "identifiers")

urlpatterns = [
    url(r'^$', cviews.Contributors.as_view(), name="contributors"),

    # Examples:
    # url(r'^$', 'dc.views.home', name='home'),
    # url(r'^dc/', include('dc.foo.urls')),

    url(r'^license/$', TemplateView.as_view(template_name='license.html'), name="root_license"),
    url(r'^about/privacy$', TemplateView.as_view(template_name='privacy.html'), name="root_privacy"),

    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^contributors/', include('contributors.urls')),
    url(r'^contributor/', include('contributor.urls')),
    url(r'^sources/', include(sources_urls.sources_urls)),
    url(r'^source/', include(sources_urls.source_urls)),
    url(r'^mia/', include("mia.urls")),
    url(r'^importer/', include("importer.urls")),
    url(r'^api/', include(router.urls)),
    url(r'^deploy/', include("deploy.urls")),
]
