from django.test import TestCase, Client
from django.core.urlresolvers import reverse
from django.core import mail, signing
from contributors import models as cmodels
from sources.test_common import *
from dc.unittest import UserFixtureMixin, SourceFixtureMixin
import datetime
import re

class ContributorTestCase(SimpleSourceFixtureMixin, DCTestUtilsMixin, TestCase):
    def test_detail(self):
        class SeesClientCheck(DCTestClientCheck):
            def __init__(self, user_tag):
                self.user_tag = user_tag
            def setup_query(self, tc, **kw):
                self.user = getattr(tc.fixture, "user_" + self.user_tag)
                tc.url_kwargs["name"] = self.user.email

        class SeesAll(SeesClientCheck):
            def check_result(self, tc):
                tc.fixture.assertEqual(tc.response.status_code, 200)
                protected_id = getattr(tc.fixture, "id_{}_home".format(self.user_tag))
                if protected_id.name not in tc.response.content.decode("utf8"):
                    tc.fixture.fail("{} cannot see full details of {}".format(
                        tc.user, protected_id))

        class SeesRedacted(SeesClientCheck):
            def check_result(self, tc):
                tc.fixture.assertEqual(tc.response.status_code, 200)
                protected_id = getattr(tc.fixture, "id_{}_home".format(self.user_tag))
                if protected_id.name in tc.response.content.decode("utf8"):
                    tc.fixture.fail("{} does show in {} when visited as {}".format(
                        protected_id.name, tc.url, tc.user))

        tc = DCTestClient(self, 'contributor_detail')

        # Supervisor can see full details on all
        for u in "admin", "dd", "dd1", "alioth", "alioth1":
            tc.assertGet(SeesAll(u), self.user_admin)

        # Others can only see full details on self
        for u in "admin", "dd", "dd1", "alioth", "alioth1":
            for vu in "dd", "dd1", "alioth", "alioth1":
                visitor = getattr(self, "user_" + vu)
                if u == vu:
                    tc.assertGet(SeesAll(u), visitor)
                else:
                    tc.assertGet(SeesRedacted(u), visitor)

    def test_settings(self):
        class UserSettingsCheck(DCTestClientCheck):
            def __init__(self, user_tag):
                self.user_tag = user_tag
            def setup_query(self, tc, **kw):
                self.user = getattr(tc.fixture, "user_" + self.user_tag)
                self.id_user = getattr(tc.fixture, "id_{}_user".format(self.user_tag))
                self.id_home = getattr(tc.fixture, "id_{}_home".format(self.user_tag))
                tc.url_kwargs["name"] = self.user.email

        class CanSee(UserSettingsCheck):
            def __init__(self, user_tag, hide_user, hide_id_user, hide_id_home):
                super(CanSee, self).__init__(user_tag)
                self.hide_user = hide_user
                self.hide_id_user = hide_id_user
                self.hide_id_home = hide_id_home

            def setup_query(self, tc, **kw):
                super(CanSee, self).setup_query(tc, **kw)
                self.user.hidden = self.hide_user
                self.user.save()
                self.id_user.hidden = self.hide_id_user
                self.id_user.save()
                self.id_home.hidden = self.hide_id_home
                self.id_home.save()

            def check_result(self, tc):
                tc.fixture.assertEqual(tc.response.status_code, 200)

                def make_input(tag, checked):
                    return '<input type="checkbox" name="hide_{}" {} class="hide_{}">'.format(
                        tag,
                        'checked="checked"' if checked else '',
                        'user' if tag == 'user' else 'ident')

                content = tc.response.content.decode("utf8")
                inputs = re.findall(r'<input type="checkbox" name="hide_[^>]+>', content)
                tc.fixture.assertEqual(len(inputs), 3)
                tc.fixture.assertEqual(inputs[0], make_input("user", self.hide_user))
                tc.fixture.assertEqual(inputs[1], make_input(self.id_user.pk, self.hide_id_user))
                tc.fixture.assertEqual(inputs[2], make_input(self.id_home.pk, self.hide_id_home))

        class CanSet(UserSettingsCheck):
            def __init__(self, user_tag, hide_user, hide_id_user, hide_id_home, missing_settings_for_visible=True):
                super(CanSet, self).__init__(user_tag)
                self.hide_user = hide_user
                self.hide_id_user = hide_id_user
                self.hide_id_home = hide_id_home
                self.missing_settings_for_visible = missing_settings_for_visible

            def setup_query(self, tc, **kw):
                super(CanSet, self).setup_query(tc, **kw)
                self.user.hidden = False
                self.user.save()
                self.id_user.hidden = False
                self.id_user.save()
                self.id_home.hidden = False
                self.id_home.save()

                if tc.data is None: tc.data = {}
                if self.hide_user: tc.data["hide_user"] = "on"
                if self.hide_id_user: tc.data["hide_{}".format(self.id_user.pk)] = "on"
                if self.hide_id_home: tc.data["hide_{}".format(self.id_home.pk)] = "on"

            def check_result(self, tc):
                from urllib.parse import quote_plus
                tc.fixture.assertEqual(tc.response.status_code, 302)
                tc.fixture.assertEqual(tc.response["Location"],
                                        "/contributor/{}/".format(self.user.unambiguous_user_id))

                saved_user = cmodels.User.objects.get(pk=self.user.pk)
                saved_id_user = cmodels.Identifier.objects.get(pk=self.id_user.pk)
                saved_id_home = cmodels.Identifier.objects.get(pk=self.id_home.pk)
                tc.fixture.assertEqual(saved_user.hidden, self.hide_user)
                tc.fixture.assertEqual(saved_id_user.hidden, self.hide_id_user)
                tc.fixture.assertEqual(saved_id_home.hidden, self.hide_id_home)


        tc = DCTestClient(self, 'contributor_detail')
        tc1 = DCTestClient(self, 'contributor_save_settings')

        for u in "admin", "dd", "dd1", "alioth", "alioth1":
            # Check that each user can see their settings
            visitor = getattr(self, "user_" + u)
            tc.assertGet(CanSee(u, False, False, False), visitor)
            tc.assertGet(CanSee(u, True, True, True), visitor)
            tc.assertGet(CanSee(u, True, False, False), visitor)
            tc.assertGet(CanSee(u, False, True, True), visitor)
            tc.assertGet(CanSee(u, False, True, False), visitor)
            tc.assertGet(CanSee(u, False, False, True), visitor)

            # Check that each user can set and retrieve their settings
            tc1.assertPost(CanSet(u, False, False, False), visitor)
            tc1.assertPost(CanSet(u, True, True, True), visitor)
            tc1.assertPost(CanSet(u, True, False, False), visitor)
            tc1.assertPost(CanSet(u, False, True, True), visitor)
            tc1.assertPost(CanSet(u, False, True, False), visitor)
            tc1.assertPost(CanSet(u, False, False, True), visitor)

    def test_user_visibility(self):
        # Anonymous or non-user, non-admin visiting user page, no hidden
        # settings, cannot see emails or other info

        # Visiting hidden user gives 404 except for same user or admin
        class WhenVisitHiddenUser(DCTestUtilsWhen):
            def setUp(self, fixture):
                super(WhenVisitHiddenUser, self).setUp(fixture)
                tu = self.args["target"]
                tu = getattr(fixture, "user_{}".format(tu))
                self.target = cmodels.User(email="tu{}".format(tu.email), hidden=True)
                self.target.save()
                # If the intention was to visit self, use the new hidden user
                # as a visiting user
                if tu == self.user:
                    self.target.remote_user = "{}:tu{}".format(*self.user.remote_user.rsplit(":", 1))
                    self.user = self.target
                self.url = reverse("contributor_detail", kwargs={ "name": self.target.email })

            def tearDown(self, fixture):
                super(WhenVisitHiddenUser, self).tearDown(fixture)
                self.target.delete()

            def __str__(self):
                return "visiting hidden user page at {}".format(self.url)

        for tu in "admin", "dd", "dd1", "alioth", "alioth1":
            self.assertVisit(WhenVisitHiddenUser(user="admin", target=tu), ThenSuccess())

        for u in "dd", "dd1", "alioth", "alioth1":
            for tu in "admin", "dd", "dd1", "alioth", "alioth1":
                if u == tu:
                    self.assertVisit(WhenVisitHiddenUser(user=u, target=tu), ThenSuccess())
                else:
                    self.assertVisit(WhenVisitHiddenUser(user=u, target=tu), ThenNotFound())


class TestImpersonate(UserFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        # Supervisor can impersonate anyone
        for visit_as in "dd", "dd1", "alioth", "alioth1":
            cls._add_method(cls._test_success, "admin", visit_as)

        # Others cannot impersonate
        for visit_as in "admin", "dd", "dd1", "alioth", "alioth1":
            for visitor in "dd", "dd1", "alioth", "alioth1":
                cls._add_method(cls._test_forbidden, visitor, visit_as)

    def _test_success(self, visitor, visit_as):
        client = self.make_test_client(visitor)
        response = client.get(reverse("contributor_impersonate", args=[self.users[visit_as].email]))
        self.assertRedirectMatches(response, reverse("contributors"))
        self.assertEqual(client.session["impersonate"], self.users[visit_as].email)

        # Trying to impersonate oneself will de-impersonate
        response = client.get(reverse("contributor_impersonate", args=[self.users[visitor].email]))
        self.assertRedirectMatches(response, reverse("contributors"))
        self.assertNotIn("impersonate", client.session)

    def _test_forbidden(self, visitor, visit_as):
        client = self.make_test_client(visitor)
        response = client.get(reverse("contributor_impersonate", args=[self.users[visit_as].email]))
        self.assertRedirectMatches(response, reverse("contributors"))
        self.assertNotIn("impersonate", client.session)


class TestUnclaim(SourceFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        for visitor in "dd", "dd1":
            for visited in "dd", "dd1", "alioth", "alioth1":
                cls._add_method(cls._test_success, visitor, visited)

        for visitor in "alioth", "alioth1":
            for visited in "dd", "dd1", "alioth", "alioth1":
                if visitor == visited:
                    cls._add_method(cls._test_success, visitor, visited)
                else:
                    cls._add_method(cls._test_forbidden, visitor, visited)

    def _test_success(self, visitor, visited):
        ident = self.idents[visited + "_user"]
        other_ident = self.idents[visited + "_home"]

        client = self.make_test_client(visitor)
        response = client.get(reverse("contributor_unclaim", args=[self.users[visited].email]))
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, '<button name="id" value="{}">'.format(ident.pk))
        self.assertContains(response, '<button name="id" value="{}">'.format(other_ident.pk))

        response = client.post(reverse("contributor_unclaim", args=[self.users[visited].email]), data={
            "id": ident.pk
        })
        self.assertRedirectMatches(response, reverse("contributor_detail", args=[self.users[visited].unambiguous_user_id]))

        self.assertIsNone(cmodels.Identifier.objects.get(pk=ident.pk).user)
        self.assertEqual(cmodels.Identifier.objects.get(pk=other_ident.pk).user, self.users[visited])

    def _test_forbidden(self, visitor, visited):
        ident = self.idents[visited + "_user"]
        other_ident = self.idents[visited + "_home"]

        client = self.make_test_client(visitor)
        response = client.get(reverse("contributor_unclaim", args=[self.users[visited].email]))
        self.assertPermissionDenied(response)

        self.assertEqual(cmodels.Identifier.objects.get(pk=ident.pk).user, self.users[visited])
        self.assertEqual(cmodels.Identifier.objects.get(pk=other_ident.pk).user, self.users[visited])

    def test_anonymous(self):
        client = Client()
        response = client.get(reverse("contributor_unclaim", args=[self.users.alioth.email]))
        self.assertPermissionDenied(response)

#    def test_unclaim(self):
#        tc = DCTestClient(self, 'contributor_unclaim', { "name": self.user_admin.email })
#        for user in (self.user_admin, self.user_dd):
#            tc.assertGet(Success(), user)
#        for user in (self.user_alioth, None):
#            tc.assertGet(Forbidden(), user)
#
#        for user in (self.user_admin, self.user_dd):
#            tc.assertPost(Redirect("/contributor/enrico@debian/"), user, data={"id": self.id_admin_user.pk})
#        for user in (self.user_alioth, None):
#            tc.assertPost(Forbidden(), user, data={"id": self.id_admin_user.pk})


class TestClaimNonDD(UserFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestClaimNonDD, cls).setUpClass()
        cls.idents.create("user", type="email", name="user@example.org")

    @classmethod
    def __add_extra_tests__(cls):
        cls._add_method(cls._test_success, "dd")
        cls._add_method(cls._test_success, "dd1")
        cls._add_method(cls._test_success, "alioth")
        cls._add_method(cls._test_success, "alioth1")
        cls._add_method(cls._test_forbidden, None)

    def _test_success(self, visitor):
        client = self.make_test_client(visitor)
        response = client.post(reverse("contributor_claim_email"), data={
            "email": self.idents.user.name,
        })
        self.assertRedirectMatches(response, client.visitor.get_absolute_url())

        signature = signing.dumps({'user': client.visitor.email, 'ident': self.idents.user.name})

        response = client.get(reverse('verify_claim'), data={"token": signature})
        self.assertRedirectMatches(response, client.visitor.get_absolute_url())

        ident = cmodels.Identifier.objects.get(pk=self.idents.user.pk)
        self.assertEqual(ident.user, client.visitor)

    def _test_forbidden(self, visitor):
        client = self.make_test_client(visitor)
        response = client.post(reverse("contributor_claim_email"), data={
            "email": self.idents.user.name,
        })
        self.assertPermissionDenied(response)

        user = client.visitor if visitor else self.users.alioth
        signature = signing.dumps({'user': user.email, 'ident': self.idents.user.name})
        self.assertPermissionDenied(response)

        ident = cmodels.Identifier.objects.get(pk=self.idents.user.pk)
        self.assertIsNone(ident.user)

    def test_claim(self):
        client = self.make_test_client("alioth")

        self.assertFalse(cmodels.Identifier.objects.filter(user=client.visitor, type="email", name="user@example.org").exists())

        response = client.post(reverse("contributor_claim_email"))
        self.assertRedirectMatches(response, client.visitor.get_absolute_url())
        self.assertEqual(len(mail.outbox), 0)

        response = client.post(reverse("contributor_claim_email"), data={"email": "user@example.org"})
        self.assertRedirectMatches(response, client.visitor.get_absolute_url())
        self.assertEqual(len(mail.outbox), 1)

        urls = [x for x in mail.outbox[0].body.splitlines() if x.startswith("http")]
        self.assertEqual(len(urls), 1)

        response = client.get(urls[0])
        self.assertRedirectMatches(response, client.visitor.get_absolute_url())

        self.assertTrue(cmodels.Identifier.objects.filter(user=client.visitor, type="email", name="user@example.org").exists())

    def test_verify(self):
        signature = signing.dumps({'user': self.users.alioth.email, 'ident': self.idents.user.name})

        client = Client()
        response = client.get(reverse('verify_claim'), data={"token": signature})
        self.assertRedirectMatches(response, self.users.alioth.get_absolute_url())

        ident = cmodels.Identifier.objects.get(pk=self.idents.user.pk)
        self.assertEqual(ident.user, self.users.alioth)
