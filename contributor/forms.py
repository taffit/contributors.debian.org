from django import forms


class NonDDAssociateEmail(forms.Form):
    email = forms.EmailField()
