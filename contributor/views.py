from django import http
from django.http import Http404
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, get_object_or_404
from django.views.generic import DetailView, TemplateView, View
from django.core.exceptions import PermissionDenied
from contributors import models as cmodels
from django.utils.translation import ugettext as _
from django.core import signing
from django.core.urlresolvers import reverse
from django.core.mail import send_mail
from django.contrib import messages
from django.conf import settings
from dc.mixins import VisitorMixin
from .forms import NonDDAssociateEmail


class ContributorMixin(VisitorMixin):
    def load_objects(self):
        super(ContributorMixin, self).load_objects()
        self.contributor = cmodels.User.objects.lookup(self.kwargs["name"])

        self.own_page = self.contributor is not None and self.visitor is not None and self.visitor.is_superuser or self.visitor == self.contributor

        # Someone else is visting this user: enforce visibility settings
        if self.contributor is not None and self.contributor.hidden and not self.own_page:
            self.contributor = None

        if self.contributor is None:
            raise Http404(_("No contributor found matching the query"))

    def check_visitor_permission(self, perm):
        if perm == "own_page":
            return self.own_page
        else:
            return super(ContributorMixin, self).check_visitor_permission(perm)

    def get_object(self):
        return self.contributor

    def get_context_data(self, **kw):
        ctx = super(ContributorMixin, self).get_context_data(**kw)
        ctx["contributor"] = self.contributor
        ctx["own_page"] = self.own_page
        return ctx


class ContributorView(ContributorMixin, DetailView):
    template_name = "contributor/contributor.html"
    context_object_name = 'contributor'

    def get_context_data(self, **kw):
        context = super(ContributorView, self).get_context_data(**kw)

        if self.own_page:
            context["user_log"] = list(self.object.log.order_by("-ts"))
            source_settings = cmodels.UserSourceSettings.objects.filter(user=self.object, hidden=True).distinct()
            identifiers = []
            for ident in self.object.identifiers.order_by("type", "name"):
                identifiers.append({
                    "ident": ident,
                    "log": ident.log.order_by("-ts"),
                })
            context["identifiers"] = identifiers
            context["source_settings"] = list(source_settings)
            context["hide_user"] = self.object.hidden

        contributions = []
        for c in self.object.contributions().order_by("identifier"):
            if self.own_page or not c.hidden:
                contributions.append(c)

        context["contributions"] = contributions

        context['redacted'] = not self.own_page
        context['form'] = NonDDAssociateEmail()
        return context


class SaveSettings(ContributorMixin, View):
    require_visitor = "own_page"

    def post(self, request, *args, **kw):
        changed = False

        # Updated user info
        full_name = self.request.POST.get("full_name", "")
        if full_name != self.contributor.full_name:
            self.contributor.full_name = full_name
            self.contributor.save()

        # Update user visibility
        want_hidden_user = bool(request.POST.get("hide_user", False))
        if self.contributor.hidden != want_hidden_user:
            self.contributor.hidden = want_hidden_user
            self.contributor.save()
            changed = True

        # Update identifier visibility
        for ident in self.contributor.identifiers.all():
            want_hidden = bool(self.request.POST.get("hide_{}".format(ident.pk), False))
            if ident.hidden != want_hidden:
                ident.hidden = want_hidden
                ident.save()
                changed = True

        if changed:
            cmodels.AggregatedPersonContribution.recompute(user=self.contributor)
            cmodels.AggregatedSource.recompute()
            cmodels.AggregatedPerson.recompute(user=self.contributor)

        return redirect(self.contributor.get_absolute_url())


class Impersonate(ContributorMixin, View):
    def get(self, request, *args, **kw):
        if "impersonate" in request.session:
            del request.session["impersonate"]

        if request.user.is_superuser and request.user != self.contributor:
            request.session["impersonate"] = self.contributor.email

        url = request.GET.get("url", None)
        if url is None:
            return redirect('contributors')
        else:
            return redirect(url)


class ClaimEmail(VisitorMixin, View):
    require_visitor = "user"

    def post(self, request, *args, **kw):
        form = NonDDAssociateEmail(request.POST)
        if form.is_valid():
            claim = form.cleaned_data['email']
            email = self.visitor.email
            signature = signing.dumps({'user': email, 'ident': claim})
            url = request.build_absolute_uri("%s?token=%s" %
                                            (reverse('verify_claim'), signature))
            message = """
Hello,

{user} (who is probably you) wants to be credited on https://contributors.debian.org
for contributions made with this email address ({claim}).

If that is correct, please confirm by clicking this link:
{url}

Thank you,

contributors.debian.org
""".format(user=email, claim=claim, url=url)
            mail_from = '{0} <{1}>'.format(settings.ADMINS[0][0], settings.ADMINS[0][1])
            send_mail('Verify contributors.debian.org claim', message,
                    mail_from, [claim], fail_silently=False)
            messages.warning(request, 'Email sent to ' + claim +
                            '. Click on the verification link to complete the process')
            return redirect(self.visitor.get_absolute_url())
        else:
            messages.warning(request, 'Invalid email')
            return redirect(self.visitor.get_absolute_url())


class VerifyClaim(VisitorMixin, View):
    def get(self, request, *args, **kw):
        try:
            loads = signing.loads(request.GET['token'], max_age=3600 * 24 * 3)
        except (signing.BadSignature, signing.SignatureExpired):
            raise http.Http404
        # find user
        email = loads['user']
        claim = loads['ident']
        try:
            person = cmodels.User.objects.get(email=email)
        except cmodels.User.DoesNotExist:
            messages.error(request, 'User %s does not exist.' % email)
            raise http.Http404
        # find identifier
        try:
            ident = cmodels.Identifier.objects.get(type='email', name=claim)
        except cmodels.Identifier.DoesNotExist:
            messages.warning(request, "Identifier email:%s does not exist" % claim)
            return redirect(person.get_absolute_url())
        if ident.user is None:
            ident.user = person
            ident.save()
            log_entry = "Associated by {} using the web interface".format(person.email)
            person.add_log(log_entry)
            ident.add_log(log_entry)
            messages.warning(request, 'Identifier %s has been associated with %s' %
                            (claim, email))
        else:
            messages.warning(request, 'Identifier %s is already associated with %s' %
                            (claim, email))
        return redirect(person.get_absolute_url())


class Unclaim(ContributorMixin, TemplateView):
    template_name = "contributor/unclaim.html"
    require_visitor = "user"

    def check_permissions(self):
        super(Unclaim, self).check_permissions()
        if not self.visitor.is_superuser and not self.visitor.is_dd and self.visitor != self.contributor:
            raise PermissionDenied

    def get_context_data(self, **kw):
        ctx = super(Unclaim, self).get_context_data(**kw)
        ctx["identifiers"] = self.contributor.identifiers.order_by("type", "name")
        return ctx

    def post(self, request, name):
        ident = get_object_or_404(cmodels.Identifier, pk=request.POST["id"])

        if ident.user is None:
            return redirect(self.contributor.get_absolute_url())

        if ident.user.pk != self.contributor.pk:
            raise PermissionDenied

        ident.user = None
        ident.save()
        log_entry = "Unassociated from {} by {} using the web interface".format(self.contributor.email, self.visitor.email)
        self.contributor.add_log(log_entry)
        ident.add_log(log_entry)

        return redirect(self.contributor.get_absolute_url())
