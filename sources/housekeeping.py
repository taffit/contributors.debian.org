import django_housekeeping as hk
from django.conf import settings
from contributors.models import Source
import datetime
import os
import logging

log = logging.getLogger(__name__)

DATA_DIR = getattr(settings, "DATA_DIR", None)

STAGES = ["backup", "main"]

class BackupContributions(hk.Task):
    """
    Make a backup of each source contribution data
    """
    def run_backup(self, stage):
        now = datetime.datetime.utcnow()
        for source in Source.objects.all():
            source.make_backup(backup_time=now)


class Thinner(object):
    """
    Evaluate datetimes for objects that need to be kept or thrown away based on
    their age. Less and less of the older items are kept
    """
    def __init__(self):
        self.seen = set()
        self.now = datetime.datetime.utcnow()

    def time_slot(self, dt):
        """
        """
        if isinstance(dt, datetime.datetime):
            age = self.now - dt
            date = dt.date()
        else:
            age = self.now.date() - dt
            date = dt

        if age.days > 365:
            return datetime.date(year=date.year, month=date.month, day=1)
        elif age.days > 60:
            return date - datetime.timedelta(days=date.weekday())
        elif age.days > 30:
            return date
        else:
            return dt

    def should_delete(self, dt):
        slot = self.time_slot(dt)
        to_delete = slot in self.seen
        self.seen.add(slot)
        return to_delete


class ThinSubmissionLogs(hk.Task):
    """
    Remove old submission logs
    """
    def run_main(self, stage):
        dry_run = self.hk.dry_run

        for source in Source.objects.all():
            thinner = Thinner()
            for pathname, dt in source.submission_log():
                to_delete = thinner.should_delete(dt)
                if dry_run:
                    print("rm" if to_delete else "ok", pathname, dt)
                elif to_delete:
                    log.info("%s: removed old %s submission log file", pathname, source.name)
                    os.unlink(pathname)


class ThinSourceBackups(hk.Task):
    """
    Remove old source backups
    """
    def run_main(self, stage):
        dry_run = self.hk.dry_run

        for source in Source.objects.all():
            thinner = Thinner()
            for info in source.backups():
                to_delete = thinner.should_delete(info["date"])
                if dry_run:
                    print("rm" if to_delete else "ok", info["datafile"], info["date"])
                    print("rm" if to_delete else "ok", info["infofile"], info["date"])
                elif to_delete:
                    log.info("%s: removed old %s source backup file", info["datafile"], source.name)
                    log.info("%s: removed old %s source backup file", info["infofile"], source.name)
                    os.unlink(info["datafile"])
                    os.unlink(info["infofile"])
