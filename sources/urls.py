from django.conf.urls import url
from . import views

sources_urls = [
    url(r'^$', views.SourcesList.as_view(), name="source_list"),
    url(r'^add/$', views.SourceCreate.as_view(), name='source_add'),
]

source_urls = [
    url(r'^(?P<sname>[^/]+)$', views.SourceView.as_view(), name='source_view'),
    url(r'^(?P<sname>[^/]+)/ctype/(?P<name>[^/]+)/$', views.CtypeView.as_view(), name='source_ctype_view'),
    url(r'^(?P<sname>[^/]+)/update/$', views.SourceUpdate.as_view(), name='source_update'),
    url(r'^(?P<sname>[^/]+)/delete/$', views.SourceDelete.as_view(), name='source_delete'),
    url(r'^(?P<sname>[^/]+)/delete/contributions/$', views.SourceDeleteContributions.as_view(), name='source_delete_contributions'),
    url(r'^(?P<sname>[^/]+)/backups/$', views.SourceBackupList.as_view(), name='source_backup_list'),
    url(r'^(?P<sname>[^/]+)/backups/(?P<backup_id>[0-9]+)$', views.SourceBackupDownload.as_view(), name='source_backup_download'),
    url(r'^(?P<sname>[^/]+)/members/$', views.SourceMembers.as_view(), name='source_members'),
    url(r'^(?P<sname>[^/]+)/members/add/$', views.SourceMembersAdd.as_view(), name='source_members_add'),
    url(r'^(?P<sname>[^/]+)/members/delete/$', views.SourceMembersDelete.as_view(), name='source_members_delete'),
    url(r'^(?P<sname>[^/]+)/ctypes/add/$', views.ContributionTypeCreate.as_view(), name='source_ctype_add'),
    url(r'^(?P<sname>[^/]+)/ctype/(?P<name>[^/]+)/update/$', views.ContributionTypeUpdate.as_view(), name='source_ctype_update'),
    url(r'^(?P<sname>[^/]+)/ctype/(?P<name>[^/]+)/delete/$', views.ContributionTypeDelete.as_view(), name='source_ctype_delete'),
    url(r'^(?P<sname>[^/]+)/ctype/(?P<name>[^/]+)/delete/contributions/$', views.ContributionTypeDeleteContributions.as_view(),
        name='source_ctype_delete_contributions'),
    url(r'^(?P<sname>[^/]+)/user-settings/$', views.SourceUserSettings.as_view(), name='source_user_settings'),
]
